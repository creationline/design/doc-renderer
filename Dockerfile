FROM surnet/alpine-wkhtmltopdf:3.13.5-0.12.6-full as wkhtmltopdf
FROM node:14-alpine AS base

# Install dependencies for wkhtmltopdf
RUN apk add --no-cache \
  libstdc++ \
  libx11 \
  libxrender \
  libxext \
  libssl1.1 \
  ca-certificates \
  fontconfig \
  freetype \
  ttf-dejavu \
  ttf-droid \
  ttf-freefont \
  ttf-liberation \
  ttf-freefont \
&& apk add --no-cache --virtual .build-deps \
  msttcorefonts-installer \
\
# Install microsoft fonts
&& update-ms-fonts \
&& fc-cache -f \
\
# Clean up when done
&& rm -rf /tmp/* \
&& apk del .build-deps

# Copy wkhtmltopdf files from docker-wkhtmltopdf image
COPY --from=wkhtmltopdf /bin/wkhtmltopdf /bin/wkhtmltopdf

RUN mkdir -p /opt/designsystem/sys/src

COPY package*.json /opt/designsystem/sys/
WORKDIR /opt/designsystem/sys

# install dependencies
RUN npm install --only=prod
ENTRYPOINT [ "npm", "run", "--silent" ]

# install dev packages
FROM base AS dev
RUN npm install
COPY tsconfig.json /opt/designsystem/sys/
CMD [ "dev" ]

# build typescript code
FROM dev AS build
COPY src/. /opt/designsystem/sys/src/
RUN npm run --silent build:sys

FROM base AS prod
RUN mkdir -p /opt/designsystem/sys/dist
COPY --from=build /opt/designsystem/sys/dist/. /opt/designsystem/sys/dist/.
ENV BIN_NODE=node BIN_PATH=./dist
CMD [ "serve" ]
# CMD [ "help" ]
