import fs from 'fs';
import { Index } from 'flexsearch';
import { SYS_DIR, TEMPLATE_DIR } from './common';
import { promisify } from "util";
import path from 'path';
import less from 'less';
import { languages } from "./definitions";

const index = {
	en: new Index({ tokenize: "reverse", language: "en" }),
	ja: new Index({ tokenize: "reverse", language: "ja" })
};

export function getTitleFromContent(path: string): string {
	const pattern = /^# .*/g;
	try {
		const f = fs.readFileSync(path, 'utf8');
		if (f.match(pattern)) {
			return f.match(pattern)[0].split('# ')[1];
		}
	} catch (e) {
		// console.error(e);
		return null;
	}
	return null;
}

export async function compileLess(outputPath: string) {
	const input = `${TEMPLATE_DIR}/css/main.less`;
	const lessFile = await fs.promises.readFile(input);
	const lessStr = lessFile.toString();
	const output = await promisify(less.render)(lessStr, { filename: path.resolve(input) });
	if (outputPath) {
		await fs.promises.writeFile(outputPath, output.css);
		console.log('✅ Less file compiled!');
		return;
	}
	return output;
}

export async function getLastModifiedLessDate() {
	const path = `${TEMPLATE_DIR}/css`;
	return await readDirectory(path)
		.then(list => list.map(path =>  ( fs.statSync(path).mtime ) ))
		.then(list => list.sort((a, b) => b.getTime() - a.getTime())[0])
}

export async function readDirectory(path: string, arr: Array<string> = []): Promise<Array<string>> {
	const files = await fs.promises.readdir(path, { withFileTypes: true });

	arr = arr || [];

	for (const file of files) {
		let fullPath = `${path}/${file.name}`;
		if (file.isDirectory()) {
			await readDirectory(fullPath, arr);
		}
		else {
			arr.push(fullPath);
		}
	}
	return arr;
}

export async function createSearchIndex() {
	for (const lang of languages) {
		try {
			const fileStr = await fs.promises.readFile(`${SYS_DIR}/data/search_db_${lang.code}.json`, 'utf8');
			const searchDb = JSON.parse(fileStr);
			for (const item of searchDb) {
				index[lang.code].add(item.id, item.content);
			}
		} catch (e) {
			if (e.code === 'ENOENT') {
				console.error('Creating search index failed. Run "npm run build" command first.');
			}
		}
	}
}

export async function search(str, lang) {
	try {
		const fileStr = await fs.promises.readFile(`${SYS_DIR}/data/search_db_${lang}.json`, 'utf8');
		const searchDb = JSON.parse(fileStr);
		const ids = index[lang].search(str, { suggest: true });
		return searchDb.filter(item => ids.includes(item.id)).map(m => {
			return { title: m.title, href: m.href }
		})
	} catch (e) {
		return e;
	}
}

export function capitalizeWord(str) {
	return `${str.charAt(0).toUpperCase()}${str.slice(1)}`;
}
