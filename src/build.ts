import express from 'express';
import nunjucks from 'nunjucks';
import fs from 'fs';
import { promisify } from 'util';
import { copy } from 'fs-extra';
import path from 'path';
import ContentService from "./content";
import { languages } from "./definitions";
import { capitalizeWord, compileLess } from "./helpers";

import {
	TEMPLATE_DIR,
	CONTENT_DIR,
	BUILD_DIR,
	IMAGE_FOLDER_NAME,
	DEFAULT_LANG, SYS_DIR,
} from "./common";

const app = express();

const njk = new nunjucks.Environment(
	new nunjucks.FileSystemLoader(TEMPLATE_DIR),
	{
		autoescape: false,
		express: app,
		noCache: true,
		watch: true,
	}
);
njk.express(app);

const content = new ContentService({
	contentDir: CONTENT_DIR,
});
let contentData = { en: [], ja: [] };

content.scan()
	.then(async () => {
		const mainMenu = content.sections;
		await moveAssets();
		await moveImages(mainMenu);
		for (const menuItem of mainMenu) {
			const leftMenu = content.sections.filter(item => item.id === menuItem.id)[0];
			for (const group of leftMenu.groups) {
				for (const page of group.pages) {
					await generatePage(mainMenu, leftMenu, menuItem, page, group.id);
				}
			}
			for (const page of leftMenu.pages) {
				await generatePage(mainMenu, leftMenu, menuItem, page, null);
			}
		}
		await buildCleanup();
		await moveIndex();
		await createSearchDbFile();
	})

async function generatePage(mainMenu, leftMenu, mainMenuItem, page, group) {
	for (const lang of page.availableLanguages) {
		const route = {
			lang,
			contentPath: `${page.path}.html`,
			section: mainMenuItem.id,
			page: page.id || 'overview',
			group
		};

		let pageHtml = null;
		try {
			pageHtml = await content.getPage(route, true);
		} catch (err) {
			console.warn('cannot fetch page:', err.message);
		}

		const tplvars = {
			languages,
			lang: route.lang || DEFAULT_LANG,
			route,
			mainMenu,
			leftMenu,
			content: pageHtml,
			isBuild: true,
			rootUrl: '../../',
			indexing: false,
		};

		let sectionDir = `${BUILD_DIR}/${lang}/${tplvars.route.section}`;
		if (tplvars.route.group) {
			sectionDir += `/${tplvars.route.group}`;
			tplvars.rootUrl += '../';
		}

		const sectionImageDir = `${sectionDir}/${IMAGE_FOLDER_NAME}`;
		if (!fs.existsSync(sectionImageDir)) {
			// create section build folders with img sub folder
			await fs.promises.mkdir(sectionImageDir, { recursive: true });
		}

		const res : string | void = await (promisify(app.render.bind(app)) as any)('index.njk', tplvars)
			.catch(err => {
				console.log(`Error rendering ${tplvars.route.contentPath}`, err);
			});

		if (!res) return;

		await fs.promises.writeFile(`${BUILD_DIR}/${lang}/${tplvars.route.contentPath}`, res)
			.catch(console.error);

		// Create search db data
		tplvars.indexing = true;
		const pageContent : string = await (promisify(app.render.bind(app)) as any)('index.njk', tplvars)
			.catch(err => {
				console.log(`Error rendering ${tplvars.route.contentPath}`, err);
			});
		const tagStripped = pageContent.replace(/<[^>]*>?/gm, '').replace(/\s/g,' ');
		contentData[lang].push({
			id: `${tplvars.route.section}-${tplvars.route.page}-${tplvars.lang}`,
			title: `${tplvars.lang.toUpperCase()} / ${capitalizeWord(tplvars.route.section)} / ${capitalizeWord(tplvars.route.page)}`,
			href: `/${tplvars.lang}/${tplvars.route.contentPath}`,
			content: tagStripped,
		});
	}
}

async function moveAssets() {
	try {
		await copy(`${TEMPLATE_DIR}/assets`, `${BUILD_DIR}/assets`);
		console.log('✅ Assets moved!');
		await compileLess(`${BUILD_DIR}/assets/main.css`);
	} catch (e) {
		console.error(e);
	}
}

async function moveImages(mainMenu) {
	try {
		for (const lang of languages) {
			for (const item of mainMenu) {
				let componentDir = `${CONTENT_DIR}/${item.path}`;
				let sectionDir = `${BUILD_DIR}/${lang.code}/${item.path}`;
				if (item.groups.length > 0) {
					for (const group of item.groups) {
						await copy(`${componentDir}/${group.id}/${IMAGE_FOLDER_NAME}` ,`${sectionDir}/${group.id}/${IMAGE_FOLDER_NAME}`);
					}
				}
				await copy(`${componentDir}/${IMAGE_FOLDER_NAME}` ,`${sectionDir}/${IMAGE_FOLDER_NAME}`);
			}
		}
		console.log('✅ Images moved!');
	} catch (e) {
		console.error(e);
	}
}

const buildFileLists = {};
const contentFileList = {};

async function buildCleanup() {
	for (const lang of languages) {
		traverseDir(`${BUILD_DIR}/${lang.code}`, function (fileName, path) {
			if (!buildFileLists[lang.code])
				buildFileLists[lang.code] = [];

			// clear filename extensions
			if (fileName.includes('.html'))
				fileName = fileName.split('.html')[0];

			buildFileLists[lang.code].push({ name: fileName, path });
		});
	}
	traverseDir(CONTENT_DIR, function (fileName, path) {
		for (const lang of languages) {
			if (!contentFileList[lang.code])
				contentFileList[lang.code] = [];

			// clear filename extensions
			if (fileName.includes('.md')) {
				if (fileName.includes(lang.code)) {
					contentFileList[lang.code].push({ name: fileName.split(`.${lang.code}`)[0], path });
				}
				continue;
			}
			contentFileList[lang.code].push({ name: fileName, path });
		}
	});
	for (const lang of languages) {
		const diffList = buildFileLists[lang.code].filter(a => !contentFileList[lang.code].some(b => a.name === b.name));
		for(const file of diffList) {
			if (file.name === 'index')
				continue;
			await fs.promises.unlink(file.path).catch(console.error);
			console.log(`File removed: ${file.name}`);
		}
	}
	console.log('✅ Build cleanup done!');
}

function traverseDir(dir, callback) {
	fs.readdirSync(dir).forEach(file => {
		let fullPath = path.join(dir, file);
		if (fs.lstatSync(fullPath).isDirectory()) {
			traverseDir(fullPath, callback);
		} else {
			callback(file, fullPath);
		}
	});
}

async function moveIndex() {
	await fs.promises.copyFile(`${TEMPLATE_DIR}/index.html` ,`${BUILD_DIR}/index.html`).catch(console.error);
	// for (const lang of languages) {
	// 	await fs.promises.copyFile(`${TEMPLATE_DIR}/index.html` ,`${BUILD_DIR}/${lang.code}/index.html`).catch(console.error);
	// }
	console.log('✅ Index.html moved!');
}

async function createSearchDbFile() {
	await fs.promises.mkdir(`${SYS_DIR}/data`, { recursive: true });
	for (const lang of languages) {
		await fs.promises.writeFile(`${SYS_DIR}/data/search_db_${lang.code}.json`, JSON.stringify(contentData[lang.code]))
			.catch(console.error);
	}
}
