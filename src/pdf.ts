import wkhtmltopdf from 'wkhtmltopdf';
import fs from 'fs';
import { BUILD_DIR, PDF_DIR } from "./common";
import { readDirectory } from './helpers';
import { languages } from "./definitions";

const bookDir = `${PDF_DIR}/book`;

function generatePdf(fileStr: string, savePath: string, fileName: string) {
	wkhtmltopdf(
		fileStr,
		{
			userStyleSheet: `${BUILD_DIR}/assets/main.css`,
			output: savePath,
			printMediaType: true,
			enableLocalFileAccess: true,
			disableSmartShrinking: true,
		}, () => {
			console.log(`✅ ${fileName} pdf generated.`);
		}
	)
}

(async function (){
	for (const lang of languages) {
		const files = await readDirectory(`${BUILD_DIR}/${lang.code}`)
			.then(list => list.filter(f => f.includes('.html')))

		files.forEach((file, i) => {
			if (file.includes('index.html')) {
				const folder = file.split(`${lang.code}/`)[1].split('/')[0];
				// ignore index.html on root
				if (folder !== 'index.html') {
					// find which folder index.html in
					const index = files.findIndex(item => item.includes(folder));
					// move index.html to beginning of the folder
					files.splice(i, 1);
					files.splice(index, 0, file);
				}
			}
		});

		if(process.env.TYPE === 'partial') {
			for (const file of files) {
				const fileStr = await fs.promises.readFile(file, 'utf8');
				const regex = /(?:[a-zA-Z-]+)[a-zA-Z]+\.[a-zA-Z]+/g;
				const fileName = regex.exec(file)[0].replace('.html', '.pdf');
				const dir = file.replace('build', 'pdf').replace(/(?:[a-zA-Z-]+)[a-zA-Z]+\.[a-zA-Z]+/g, '');
				await fs.promises.mkdir(dir, { recursive: true });
				if (fileStr.includes('<html>'))
					generatePdf(fileStr, `${dir}${fileName}`, `${fileName} ${lang.code}`);
			}
		}
		else {
			let fileStr = '';
			for (const file of files) {
				fileStr += await fs.promises.readFile(file, 'utf8');
			}
			await fs.promises.mkdir(bookDir, { recursive: true });
			generatePdf(fileStr, `${bookDir}/design-book-${lang.code}.pdf`, lang.code);
		}
	}
})()

console.warn('⚠️  Run `npm run build` if build html files does not exists');
console.log('⌛ PDF generator is running...');
