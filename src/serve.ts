// stdlib
import http     from 'http';

// modules
import express  from 'express';
import nunjucks from 'nunjucks';
import cookieParser from 'cookie-parser';

import ContentService from './content';
import { compileLess, createSearchIndex, getLastModifiedLessDate, search } from "./helpers";

import {
	// types
	MenuItem,
	Route,
	Language,

	// consts
	languages, ContentSection,
} from './definitions';

import {
	TEMPLATE_DIR,
	CONTENT_DIR,
	DEFAULT_LANG,
} from "./common";

// Express App
const app = express();
app.use(cookieParser())

const content = new ContentService({
	contentDir: CONTENT_DIR,
});

declare global {
	namespace Express {
		export interface Request {
			tplvars?: {
				languages : Language[],
				lang      : string,
				mainMenu? : MenuItem[],
				leftMenu? : ContentSection,
				content?  : string,
				route?    : Route,
				rootUrl   : string,
			},
		}
	}
}

// nunjucks, Template Renderer
const njk = new nunjucks.Environment(
	new nunjucks.FileSystemLoader(TEMPLATE_DIR),
	{
		autoescape: false,
		express: app,
		noCache: true,
		watch: true,
	}
);
njk.express(app);
createSearchIndex();

// Landing page
app.get('/', function(req, res) {
	// TODO: Get default landing page from configuration
	res.redirect(`/${DEFAULT_LANG}/design`);
});

// serve tpl folder as-is for now, for static files
app.use('/', express.static(TEMPLATE_DIR));
app.use('/~content', express.static(CONTENT_DIR));

const contentRouter = express.Router();

const contentScanningPromise = content.scan()
.catch(err => {
	console.error('Scanning content failed:', err);
	process.exit(1);
});

app.get('/search', async (req, res) => {
	const key = req.query.key;
	const resp = await search(key, req.cookies.lang);
	if (resp.code === 'ENOENT') {
		res.status(500).send({ error: 'An error has been occurred' });
		return;
	}
	res.send(resp);
})

contentRouter.use(async (req, res, next) => {
	// Only allow extentionless URLs, or URLs with `.html` extension
	const extensionMatch = /\.([^/?]+)(:?\?.*)?$/.exec(req.url);
	if (extensionMatch && extensionMatch[1] && extensionMatch[1] !== 'html') {
		next();
		return;
	}

	await contentScanningPromise;

	const route_parts = req.url
		.replace(/\.html$/, '')
		.split('/')
		.filter(v => !!v);

	const route: Route = {
		lang        : route_parts.shift(),

		// remaining parts after `lang`
		contentPath : route_parts.join('/'),

		section     : route_parts.shift(),
		page        : route_parts.pop() || 'index',
		group       : route_parts.shift(), // || null
	};

	let pageHtml = null;
	try {
		pageHtml = await content.getPage(route);
	} catch (err) {
		console.warn('cannot fetch page:', err.message);
		next();
		return;
	}

	// Common template data
	req.tplvars = {
		languages,
		route,
		lang     : route.lang || DEFAULT_LANG,
		mainMenu : content.sections,
		leftMenu : content.sections.filter(item => item.id === route.section)[0],
		content  : pageHtml,
		rootUrl  : '/',
	};

	res.cookie('lang', route.lang, { maxAge: 1000*60*60*24*365, httpOnly: true });
	res.render('index.njk', req.tplvars);
});

app.use('/', contentRouter);

app.get('/assets/main.css', async (req, res) => {
	const clientLastModified = new Date(req.header('if-modified-since'));
	const serverLastModified = await getLastModifiedLessDate();
	res.header("Content-Type", "text/css");
	res.header("Cache-Control", "no-cache");
	res.setHeader('Last-Modified', new Date(serverLastModified).toISOString());
	if (clientLastModified.getTime() == serverLastModified.getTime()) {
		res.sendStatus(304);
		return;
	}
	console.log('Less compiled on the fly!');
	const less = await compileLess(null);
	res.end(less.css);
})

const http_server = http.createServer(app);
http_server.listen(9090, (...args) => {
	console.log('listening on', http_server.address());
});
