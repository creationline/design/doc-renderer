/*
  Marked.js extension for kramdown style image attributes
*/

export const imageExtension = {
	name: 'image',
	level: 'inline',

	// watch for `![` for start hint
	start(src) { return src.match(/!\[/)?.index; },

	// parse token
	tokenizer(src: string, tokens: any[]) {
		const match = /^\s*!\[([^\]]*)\]\(([^)]+)\)(?:{:\s*([^\}]+)\s*})?\s*$/.exec(src);

		if (!match) return;

		return {
			type  : 'image',
			raw   : src,
			title : match[1],
			text  : match[1],
			href  : match[2],
			attr  : !match[3] ? {} : match[3]
				.split(/\s+/)
				.map(a => {
					// class
					if (a[0] === '.')
						return { class: a.substr(1).split('.') };

					const kv = /^([^=]+)=(.+)$/.exec(a);
					if (kv) {
						return { [kv[1]]: kv[2] };
					}

					// unknown/flag
					return a;
				})
				.reduce((obj: any, kv: any) => ({ ...obj, ...kv }), {}),
		};
	},

	// render img tag
	renderer(token) {
		const xml_attrs = {
			...token.attr,

			// TODO: use marked/helpers.cleanurl
			src: token.href,
			alt: token.title,
		};

		const xml_attrs_str = Object.entries(xml_attrs)
			// TODO: escape `val`
			.map(([ name, val ]) => `${name}="${val}"`)
			.join(' ');

		return `<img ${xml_attrs_str} />`;
	},
};
