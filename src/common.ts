
export const TEMPLATE_DIR = `${__dirname}/../../tpl`;
export const CONTENT_DIR = `${__dirname}/../../content`;
export const BUILD_DIR = `${__dirname}/../../build`;
export const PDF_DIR = `${__dirname}/../../pdf`;
export const SYS_DIR = `${__dirname}/../../sys`;
export const IMAGE_FOLDER_NAME = 'img';

export const DEFAULT_LANG = 'ja';
