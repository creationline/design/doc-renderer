import fs from 'fs';

import Marked from 'marked';
import yaml from 'js-yaml';

import {
	Route,
	ContentSection,
	ContentGroup,
	ContentPage,
	ContentSectionMeta,
	MultiLangString,
	languages
} from './definitions';

import { imageExtension } from './md/extension.image';
import { getTitleFromContent } from './helpers';
import { BUILD_DIR } from "./common";

export default class ContentService {
	contentDir : string;
	sections   : ContentSection[];
	marked     : any = Marked;

	constructor({ contentDir }) {
		this.contentDir = contentDir;

		this.marked.use({ xhtml: true });
		this.marked.use({ renderer: {
			link: this.renderLink.bind(this),
		}});
		this.marked.use({ extensions: [ imageExtension ] });
	}

	renderLink(url, title, text) {
		const isExternal = /^(?:[a-zA-Z0-9-_+]+:)\/\//.test(url);
		if (!isExternal) {
			const urlWithoutExtension = url.replace(/.\w+.md/g, '.html');
			return `<a class="internal" href="${urlWithoutExtension}">${text}</a>`;
		}

		return `<a class="external" href="${url}">${text}</a>`;
	}

	async scan(): Promise<ContentSection[]> {
		// TODO: Scan (TODO: migrate from build code)

		// TODO: Define ContentSection interface
		//       * pages: ContentPage[]
		// TODO: Define ContentGroup interface
		//       * pages: ContentPage[]
		// TODO: Define ContentPage interface
		//       * content?: string
		//       * lang    : LanguageCode
		// TODO: all expected to extend from a common interface
		//       having;
		//       * id
		//       * title: MultiLangString
		//       * path: string # filesystem path

		// each folder in content => section
		//   this.sections.push(ContentSection)
		// read `meta.yaml` in each section
		//   read groups in section from that file as well
		//   section.groups.push(ContentGroup)
		// each folder in section is a group
		//   verify folder existence for each group
		// each file in section/group is a page
		//   this.pages.push(ContentPage)
		//     or? section.pages.push()
		//     and/or? group.pages.push()
		//   parse page title (from first heading in md)
		// each variation of page is a available langauge (code)
		const sections: ContentSection[] = [];
		const folderList = await fs.promises.readdir(this.contentDir, { withFileTypes: true })
			.then(list => list.filter(d => d.isDirectory()))
			.then(list => list.map(d => d.name));

		for (const folder of folderList) {
			// TODO: Consider runtime validation
			const meta: ContentSectionMeta = yaml.load(await fs.promises.readFile(`${this.contentDir}/${folder}/meta.yaml`, 'utf8'));
			const groups: ContentGroup[] = [];
			const pages: ContentPage[] = [];

			if (meta.defaultPage) {
				pages.push({
					id    : meta.defaultPage.id,
					title : meta.defaultPage.title,
					path  : `${folder}/index`,
					availableLanguages: languages.map(l => l.code), // TODO: Actually populate this from files in content
				});
			}

			if (meta.groups) {
				for (const groupMeta of meta.groups) {
					const groupPages: ContentPage[] = [];
					const groupFolderFiles = await fs.promises.readdir(`${this.contentDir}/${folder}/${groupMeta.id}`, { withFileTypes: true })
						.then(list => list.filter(d => !d.isDirectory()))
						.then(list => list.map(d => d.name));

					for (const file of groupFolderFiles) {
						const title: MultiLangString = {};
						const langs = [];
						const fileName = file.split('.')[0];
						const isPageInList = groupPages.findIndex(item => item.id === fileName) !== -1;
						if (file.includes('.md') && !isPageInList) {
							for (const lang of languages) {
								const contentTitle = getTitleFromContent(`${this.contentDir}/${folder}/${groupMeta.id}/${fileName}.${lang.code}.md`);
								if (contentTitle)
									langs.push(lang.code);
								title[lang.code] = contentTitle;
							}
							groupPages.push({
								id    : fileName,
								title,
								path  : `${folder}/${groupMeta.id}/${fileName}`,
								availableLanguages: langs,
							});
						}
					}
					groups.push({
						...groupMeta,
						pages: groupPages,
					});
				}
			}

			const folderFiles = await fs.promises.readdir(`${this.contentDir}/${folder}`, { withFileTypes: true })
				.then(list => list.filter(d => !d.isDirectory()))
				.then(list => list.map(d => d.name));

			for (const file of folderFiles) {
				const title: MultiLangString = {};
				const langs = [];
				const fileName = file.split('.')[0];
				const isPageInList = pages.findIndex(item => item.id === fileName) !== -1;
				if (file.includes('.md') && !isPageInList) {
					for (const lang of languages) {
						// TODO: Check existence
						const contentTitle = getTitleFromContent(`${this.contentDir}/${folder}/${fileName}.${lang.code}.md`);
						if (contentTitle)
							langs.push(lang.code);
						title[lang.code] = contentTitle;
						// TODO: populate global available language list
					}
					pages.push({
						id    : fileName,
						title,
						path  : `${folder}/${fileName}`,
						availableLanguages: langs,
						// TODO: lang
					});
				}
			}
			sections.push({
				...meta,
				id     : folder,
				path   : `${folder}`,
				index  : 'index',
				groups,
				pages,
			});
		}

		sections.sort((a, b) => a.order - b.order);

		this.sections = sections;
		return sections;
	}

	// TODO: async
	async getPage(route: Route, isBuild: Boolean = false): Promise<string> {
		let parent_path = `${route.section}`;
		if (route.group)
			parent_path += `/${route.group}`;

		const section = this.sections.find(s => s.id === route.section);
		const filename: String = (route.page === 'index') ? section.defaultPage.id : route.page;
		const path = `${this.contentDir}/${parent_path}/${filename}.${route.lang}.md`;

		try {
			const file = await fs.promises.readFile(path, 'utf8');

			let html = this.marked(file);
			if (!isBuild)
				html = html.replace(/src="/g, `src="/~content/${parent_path}/`);
			// else
			// 	html = html.replace(/src="/g, `src="${BUILD_DIR}/${route.lang}/${parent_path}/`);

			return html;
		} catch (err) {
			console.error(`Cannot get page, ${path}`, err);
			throw err;
		}
	}
}
