
// Multilang Support

export type LanguageCode = string;

export interface Language {
	code        : LanguageCode;
	name        : string;
	englishName : string;
}

export interface MultiLangString {
	[lang: string]: string;
}

// TODO: move to config
export const languages: Language[] = [
	// {
	// 	code        : 'en',
	// 	name        : 'English',
	// 	englishName : 'English',
	// },
	{
		code        : 'ja',
		name        : '日本語',
		englishName : 'Japanese',
	},
];

// Menu

export interface MenuItem {
	id        : string;
	title     : MultiLangString;
}

export interface MenuGroup extends MenuItem {
	menuItems : MenuItem[];
}

export type MenuElement = MenuItem | MenuGroup;

export interface Route {
	lang        : LanguageCode,
	section     : string,
	group?      : string,
	page        : string,

	contentPath : string, // section + group? + page
}

export interface ContentPageMeta {
	id: string,
	title: MultiLangString,
}

export interface ContentPage extends ContentPageMeta {
	path: string,
	availableLanguages: Array<string>,
	// content: string,
	// lang: LanguageCode,
}

export interface ContentGroupMeta {
	id: string,
	title: MultiLangString,
}

export interface ContentGroup extends ContentGroupMeta {
	pages: ContentPage[],
}

export interface ContentSectionMeta {
	defaultPage? : MenuItem, // TODO: just an id would be better?
	title        : MultiLangString,
	order        : number,
	groups?      : ContentGroupMeta[],
}

export interface ContentSection extends ContentSectionMeta {
	id: string,
	path: string,
	groups: ContentGroup[] | null,
	pages: ContentPage[],
	index: string,
}
